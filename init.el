;; Pyero's emacs 
;; This config is made by PyerineVanboline
;; is under GPLv3
;; and started on june 27th 2021


;;(setq startup/gc-cons-threshold gc-cons-threshold)
;;(setq gc-cons-threshold most-positive-fixnum)
;;(defun startup/reset-gc () (setq gc-cons-threshold startup/gc-cons-threshold))
;;(add-hook 'emacs-startup-hook 'startup/reset-gc)

;; Packages mirrors
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
   

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; Start up Performace

(setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                    (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

;; remove UI emacs 
(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room
(menu-bar-mode -1)            ; Disable the menu bar

;; remove the ugly start menu
(setq inhibit-startup-message t)


;; Numbers
(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                vterm-mode-hook
                treemacs-mode-hook
                eshell-mode-hook
		eww-mode-hook
		image-mode-hook
		dired-mode-hook
		calc-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq visible-bell t)

;; Search words
;;This is a better program for search words in Emacs
(use-package swiper)
;; Which key
;;Is for which a key, when you forgot a key
  (use-package which-key
    :defer 0
    :diminish which-key-mode
    :config
    (which-key-mode)
    (setq which-key-idle-delay 1))
; Apparence
;; Theme

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-Iosvkem t)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; or for treemacs users
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;; Tranparency
(defvar efs/frame-transparency '(83 . 83))

(set-frame-parameter (selected-frame) 'alpha efs/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,efs/frame-transparency))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Font 
;;(set-face-attribute 'default nil :font "VictorMono Nerd Font" :height 140)
(add-to-list 'default-frame-alist
             '(font . "VictorMono Nerd Font-15"))

;; A better modeline
(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 10)))

;; Icons
;;This is for Emacs Icons and related with programing stuff
(use-package all-the-icons)

; Raindow delimeters
;;This is for Emacs lisp, and variants of lisp. I think is pretty useful with other lenguages
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

; Text manipulation
;;Evil mode (For Vim users)
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config 
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)  
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
 (evil-collection-init))

;; Org mode
(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(defun efs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))


    ;; Ensure that anything that should be fixed-pitch in Org files appears that way
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-agenda-files
        '("~/OrgFiles/Tasks.org"))
  
  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60)
  (setq org-todo-keywords
	'((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
          (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))
  
  (setq org-refile-targets
	'(("Archive.org" :maxlevel . 1)
          ("Tasks.org" :maxlevel . 1)))
  
  ;; Save Org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)
  
  (setq org-tag-alist
	'((:startgroup)
; Put mutually exclusive tags here
          (:endgroup)
          ("@errand" . ?E)
          ("@home" . ?H)
          ("@work" . ?W)
          ("agenda" . ?a)
          ("planning" . ?p)
          ("publish" . ?P)
          ("batch" . ?b)
          ("note" . ?n)
          ("idea" . ?i)))
  
  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
	'(("d" "Dashboard"
	   ((agenda "" ((org-deadline-warning-days 7)))
            (todo "NEXT"
		  ((org-agenda-overriding-header "Next Tasks")))
            (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))
	  
	  ("n" "Next Tasks"
	   ((todo "NEXT"
		  ((org-agenda-overriding-header "Next Tasks")))))
	  ("W" "Work Tasks" tags-todo "+work-email")
	  ;; Low-effort next actions
	  ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
	   ((org-agenda-overriding-header "Low Effort Tasks")
            (org-agenda-max-todos 20)
            (org-agenda-files org-agenda-files)))
	  
	  ("w" "Workflow Status"
	   ((todo "WAIT"
		    ((org-agenda-overriding-header "Waiting on External")
		     (org-agenda-files org-agenda-files)))
              (todo "REVIEW"
		    ((org-agenda-overriding-header "In Review")
		     (org-agenda-files org-agenda-files)))
              (todo "PLAN"
		    ((org-agenda-overriding-header "In Planning")
		     (org-agenda-todo-list-sublevels nil)
		     (org-agenda-files org-agenda-files)))
              (todo "BACKLOG"
		    ((org-agenda-overriding-header "Project Backlog")
		     (org-agenda-todo-list-sublevels nil)
		     (org-agenda-files org-agenda-files)))
              (todo "READY"
		    ((org-agenda-overriding-header "Ready for Work")
		     (org-agenda-files org-agenda-files)))
              (todo "ACTIVE"
		    ((org-agenda-overriding-header "Active Projects")
		     (org-agenda-files org-agenda-files)))
              (todo "COMPLETED"
		    ((org-agenda-overriding-header "Completed Projects")
		     (org-agenda-files org-agenda-files)))
              (todo "CANC"
		    ((org-agenda-overriding-header "Cancelled Projects")
		     (org-agenda-files org-agenda-files)))))))
    
    (setq org-capture-templates
	  `(("t" "Tasks / Projects")
            ("tt" "Task" entry (file+olp "~/OrgFiles/Tasks.org" "Inbox")
             "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)
	    
            ("j" "Journal Entries")
            ("jj" "Journal" entry
             (file+olp+datetree "~/OrgFiles/Journal.org")
             "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
             ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
             :clock-in :clock-resume
             :empty-lines 1)
            ("jm" "Meeting" entry
             (file+olp+datetree "~/OrgFiles/Journal.org")
             "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
             :clock-in :clock-resume
             :empty-lines 1)

            ("w" "Workflows")
            ("we" "Checking Email" entry (file+olp+datetree "~/OrgFiles/Journal.org")
             "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)
	    
        ("m" "Metrics Capture")
        ("mw" "Weight" table-line (file+headline "~/OrgFiles/Metrics.org" "Weight")
         "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))

    (define-key global-map (kbd "C-c j")
      (lambda () (interactive) (org-capture nil "jj")))

    (efs/org-font-setup))

(use-package org-bullets
    :after org
    :hook (org-mode . org-bullets-mode)
    :custom
    (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

  ;; Org babel
(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

;; Dashboard

;; Or if you use use-package
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

;; Set the title
(setq dashboard-banner-logo-title "Welcome Pyero!!")
;; Set the banner
(setq dashboard-startup-banner "~/.emacs.d/computer.png")
;; Value can be
;; 'official which displays the official emacs logo
;; 'logo which displays an alternative emacs logo
;; 1, 2 or 3 which displays one of the text banners
;; "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer

;; Content is not centered by default. To center, set
(setq dashboard-center-content t)

;; To disable shortcut "jump" indicators for each section, set
(setq dashboard-show-shortcuts t)

(setq dashboard-set-navigator t)

(setq dashboard-set-heading-icons t)

(setq dashboard-set-file-icons t)

(setq dashboard-items '((recents  . 3)
                        (bookmarks . 3)
                        (projects . 3)
                        (agenda . 3)))

(setq dashboard-set-init-info t)

(setq dashboard-init-info "Welcome to your favorite tool for write beautiful computer programs")

(setq dashboard-footer-messages '("Don't forget, be productive and happy Hacking!!"))
(setq dashboard-footer-icon (all-the-icons-octicon "dashboard"
                                                   :height 1.1
                                                   :v-adjust -0.02
                                                   :face 'font-lock-keyword-face))

;; Rss Feed
;; An RSS/Atom feed reader for emacs

(use-package elfeed
  :bind ("C-x w" . elfeed))

;;  Pull in elfeed-org for storing feed configuration in an org file
(use-package elfeed-org)
(require 'elfeed-org)
(elfeed-org)
(setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org"))

;;  n elfeed-goodies for even more goodies

(use-package elfeed-goodies
  :config
  (elfeed-goodies/setup))



;Pomodoro
;(require 'org)
;(setq org-clock-sound "~/Downloads/sound.wav")

; Documentation
;; Helpful
;;This is for see the documentation, of any function in Emacs 
(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

					; Keybindings
;; Counsel
(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

;; General 
(use-package general
  :config
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (rune/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "tt" '(calc :which-key "calculator")
    "sb" '(switch-to-buffer :which-key "switch buffer")
    "ag" '(org-agenda :which-key "agenda")
    "[" '(enlarge-window-horizontally :which-key "enlarge")))
;; Hydra
(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(rune/leader-keys "ts" '(hydra-text-scale/body :which-key "scale text"))

;; Ivy
;;; Ivy config
(use-package ivy
    :diminish
    :bind (("C-s" . swiper)
           :map ivy-minibuffer-map
           ("TAB" . ivy-alt-done)	
           ("C-l" . ivy-alt-done)
           ("C-j" . ivy-next-line)
           ("C-k" . ivy-previous-line)
           :map ivy-switch-buffer-map
           ("C-k" . ivy-previous-line)
           ("C-l" . ivy-done)
           ("C-d" . ivy-switch-buffer-kill)
           :map ivy-reverse-i-search-map
           ("C-k" . ivy-previous-line)
           ("C-d" . ivy-reverse-i-search-kill))
    :config
    (ivy-mode 1))
;; Ivy-rich
;;This package comes with rich transformers for commands from ivy and counsel. It should be easy enough to define your own transformers too.
(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))
					; Music

(use-package mingus)
(require 'mingus)

; Projectile and Magit
; This is for git, and organize projects.
;; Projectile
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

;; Magit
(use-package magit
  :custom
    (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
;; NOTE: Make sure to configure a GitHub token before using this package!
;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started
(use-package forge
  :after magit)

; File management
;;This is a cool program for manage files
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer))

(use-package dired-single)

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dired-open
  :config
  ;; Doesn't work as expected!
  ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
  (setq dired-open-extensions '(("epub" . "zathura")
				("pdf" . "zathura")
                                ("mkv" . "mpv")
                                ("mp4" . "mpv")
                                ("pptx" . "libreoffice")
                                ("docx" . "libreoffice"))))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
  "H" 'dired-hide-dotfiles-mode))

(setq gc-cons-threshold (* 2 1000 1000))

;(require 'multiple-cursors)
;(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
;(global-set-key (kbd "C->") 'mc/mark-next-like-this)
;(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)


;; maths
(defun update-org-latex-fragment-scale ()
  (let ((text-scale-factor
         (expt text-scale-mode-step text-scale-mode-amount)))
      (plist-put org-format-latex-options
                 :scale (* 1.2 text-scale-factor)))
    )
(add-hook 'text-scale-mode-hook 'update-org-latex-fragment-scale)

;; fira code
;; Enable the www ligature in every possible major mode
					;(use-package fira-code-mode
					;  :hook prog-mode)
					; mode to enable fira-code-mode in

